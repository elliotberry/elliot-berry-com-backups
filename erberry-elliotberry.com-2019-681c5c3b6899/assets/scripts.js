var documentTitle = document.title + " - ";

document.addEventListener("DOMContentLoaded", function() {
    doNotScrapeMyEmailAddress();
    titleMarquee();
});
let doNotScrapeMyEmailAddress = function() {
    let weirdArray = ["m", "o", "c", ".", "y", "r", "r", "e", "b", "t", "o", "i", "l", "l", "e", "@", "e", "m"];
    let theAnswer = weirdArray.reverse().join('');
    let template = `
    <a href="mailto:` + theAnswer + `">` + theAnswer + `</a>`;
    document.querySelector('#stuff').innerHTML = template;  
}

let titleMarquee = function() {
    document.title = documentTitle = documentTitle.substring(1) + documentTitle.substring(0,1);
    setTimeout(titleMarquee, 200);
}